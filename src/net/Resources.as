package net 
{

	public class Resources 
	{
		
		[Embed(source = "../../lib/graphics/player.png")]
		public static var imgPlayer:Class;
	
		[Embed(source = "../../lib/graphics/animation.png")]
		public static var imgAnimation:Class;
	}

}