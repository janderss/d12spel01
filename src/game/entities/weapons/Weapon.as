package game.entities.weapons 
{
	import net.flashpunk.Entity;

	public class Weapon 
	{
		public var owner:Entity;
		
		public var cooldown:int = 30;
		private var _cooldownTimer:int = 0;
		
		public var evHandleShooting:Function;
		
		
		public function Weapon(_owner:Entity, _evHandleShooting:Function = null) 
		{
			owner = _owner;
			evHandleShooting = _evHandleShooting;
		}
		/**
		 * Använd denna funktion en gång per frame
		 * @param shoot True om du skjuter denna frame
		 * @param shootDirX mot vilket håll du vill skjuta på X axeln
		 * @param shootDirX mot vilket håll du vill skjuta på Y axeln
		 */
		public function update():void
		{
		if (_cooldownTimer > 0) {
				_cooldownTimer--;
			} else {
			if (evHandleShooting != null) {
					evHandleShooting();
				}
			}
		}
		
		/**
		 * Denna funktion körs varje gång vapnet skall skjuta.
		 * Skriv över denna funktion.
		 */
		
		public function shoot(dirX:Number, dirY:Number):void
		{
			 _cooldownTimer = cooldown;
		}
		
	}

}