package game.entities.weapons 
{
	import game.entities.bullet;
	import net.flashpunk.Entity;

	public class WeaponShotgun extends Weapon
	{
		
		public function WeaponShotgun(_owner:Entity, _evHandleShooting:Function = null) 
		{
			super(_owner, _evHandleShooting);
			
			cooldown = 15
		}
		
		override public function shoot(dirX:Number, dirY:Number):void
		{
			// Lägg till skote till världen!
		    for (var i:int = 0; i < 10; i++) { 
			
			var b:bullet = new bullet(this,owner.x, owner.y,  dirX - 
			0.2 + 0.4 * Math.random(), dirY - 0.2 + 0.4 * Math.random());
			
			
			b.damage = 0.5;
			
			owner.world.add(b);
			}
			super.shoot(dirX, dirY);
		
		}	
		
	}

}