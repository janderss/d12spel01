package game.entities.weapons 
{
	import game.entities.bullet;
	import net.flashpunk.Entity;

	public class WeaponPistol extends Weapon
	{
		
		public function WeaponPistol(_owner:Entity, _evHandleShooting:Function = null) 
		{
			super(_owner, _evHandleShooting);
			
			cooldown = 10;
		}
		
		override public function shoot(dirX:Number, dirY:Number):void
		{
			// Lägg till skote till världen!
			owner.world.add(new bullet(this,owner.x, owner.y,  dirX - 
			0.05 + 0.1 * Math.random(), dirY - 0.05 + 0.1 * Math.random()));
			
			super.shoot(dirX, dirY);
		}
	}

}