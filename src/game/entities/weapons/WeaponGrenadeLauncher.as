package game.entities.weapons 
{
	import game.entities.bullet;
	import game.entities.BulletGrenade;
	import net.flashpunk.Entity;

	public class WeaponGrenadeLauncher extends Weapon
	{
		
		public function WeaponGrenadeLauncher(_owner:Entity, _evHandleShooting:Function = null) 
		{
			super(_owner, _evHandleShooting);
			
			
			cooldown = 30;
		}
		
		override public function shoot(dirX:Number, dirY:Number):void
		{
			// Lägg till skote till världen!
			
			owner.world.add(new BulletGrenade(this,owner.x, owner.y,  dirX - 
			0.1 + 0.2 * Math.random(), dirY - 0.1 + 0.2 * Math.random()));
			
			super.shoot(dirX, dirY);
			
		}
	}

}