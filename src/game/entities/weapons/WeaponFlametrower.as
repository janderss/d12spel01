package game.entities.weapons 
{
	import game.entities.Flame;
	import game.entities.FlameGrenade;
	import net.flashpunk.Entity;

	public class WeaponFlametrower extends Weapon
	{
		
		public function WeaponFlametrower(_owner:Entity, _evHandleShooting:Function = null) 
		{
			super(_owner, _evHandleShooting);
			
			cooldown = 0
		}
		
		override public function shoot(dirX:Number, dirY:Number):void
		{
			// Lägg till skote till världen!
		    for (var i:int = 0; i < 20; i++) { 
			
			owner.world.add(new Flame(this,owner.x, owner.y,  dirX - 
			0.1 + 0.2 * Math.random(), dirY - 0.1 + 0.2 * Math.random()));
			}
			
			super.shoot(dirX, dirY);
		
			
		}
	}

}