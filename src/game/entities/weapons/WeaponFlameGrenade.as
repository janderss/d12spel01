package game.entities.weapons 
{
	import game.entities.FlameGrenade;
	import game.entities.Flame;
	import net.flashpunk.Entity;

	public class WeaponFlameGrenade extends Weapon
	{
		
		public function WeaponFlameGrenade(_owner:Entity, _evHandleShooting:Function = null) 
		{
			super(_owner, _evHandleShooting);
			
			
			cooldown = 30;
		}
		
		override public function shoot(dirX:Number, dirY:Number):void
		{
			// Lägg till skote till världen!
			
			owner.world.add(new FlameGrenade(this,owner.x, owner.y,  dirX - 
			0.1 + 0.2 * Math.random(), dirY - 0.1 + 0.2 * Math.random()));
			
			super.shoot(dirX, dirY);
			
		}
	}

}