package game.entities.weapons 
{
	import game.entities.bullet;
	import net.flashpunk.Entity;

	public class WeaponAk extends Weapon
	{
		
		public function WeaponAk(_owner:Entity, _evHandleShooting:Function = null) 
		{
			super(_owner, _evHandleShooting);
			
			
			cooldown = 2;
		}
		
		override public function shoot(dirX:Number, dirY:Number):void
		{
			// Lägg till skote till världen!
			
			owner.world.add(new bullet(this,owner.x, owner.y,  dirX - 
			0.1 + 0.2 * Math.random(), dirY - 0.1 + 0.2 * Math.random()));
			
			super.shoot(dirX, dirY);
			
		}
	}

}