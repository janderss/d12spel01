package game.entities 
{
	import game.entities.weapons.Weapon;
	import net.flashpunk.graphics.Image;

	public class FlameGrenade extends Flame
	{
		
		public function FlameGrenade(_weapon:Weapon,_x:int, _y:int, _dirX:Number, _dirY:Number) 
		{
		super(_weapon,_x, _y, _dirX, _dirY);
		
			graphic = Image.createRect(14, 14, 0xF00000);
			graphic.x -= 20;
			graphic.y -= 20;
			
			setHitbox(14, 14, 7, 7);
			
		time = 80;
		speed = 20;
		}
		
		override public function remove():void
		{
			for (var i:int = 0; i < 3000; i++) {
			/*	
			 // Ruti spridning
			  var b:bullet = new bullet(weapon, x, y,
					-1 + 2 * Math.random(), -1 + 2 * Math.random());
					*/
					
			 // Cirkel spridning
			  var a:Number = i / 3000 * Math.PI * 8;
			  var b:Flame = new Flame(weapon, x, y,
				   Math.cos(a), Math.sin(a));
			 
			b.speed = 15 * Math.random();
			b.time = 30;
			
			weapon.owner.world.add(b);		
		}
			super.remove()
		}
	}

}