package game.entities 
{
	import game.entities.weapons.Weapon;
	import net.flashpunk.graphics.Image;

	public class BulletGrenade extends bullet
	{
		
		public function BulletGrenade(_weapon:Weapon,_x:int, _y:int, _dirX:Number, _dirY:Number) 
		{
		super(_weapon,_x, _y, _dirX, _dirY);
		
			graphic = Image.createRect(14, 14, 0x0000FF);
			graphic.x -= 7;
			graphic.y -= 7;
			
			setHitbox(14, 14, 7, 7);
			
		time = 30;
		speed = 10;
		}
		
		
		
		override public function remove():void
		{
			for (var i:int = 0; i < 54; i++) {
			/*	
			 // Ruti spridning
			  var b:bullet = new bullet(weapon, x, y,
					-1 + 2 * Math.random(), -1 + 2 * Math.random());
					*/
					
			 // Cirkel spridning
			  var a:Number = i / 54 * Math.PI * 2;
			  var b:bullet = new bullet(weapon, x, y,
				   Math.cos(a), Math.sin(a));
			 
			b.speed = 6 * Math.random();
			b.time = 16;
			
			weapon.owner.world.add(b);		
		}
			super.remove()
		}
	}

}