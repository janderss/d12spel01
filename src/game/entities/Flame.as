package game.entities 
{
	import game.entities.weapons.Weapon;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;

	public class Flame extends Entity
	{
		public var weapon:Weapon;
		
		public var speed:Number = 15;
		
		public var dirX:Number;
		public var dirY:Number;
		
		public var friction:Number = 0.90;
		public var time:int = 10;
		
		public function Flame(_weapon:Weapon, _x:int, _y:int, _dirX:Number, _dirY:Number) 
		{
			super(_x, _y);
			
			weapon = _weapon;
			
			dirX = _dirX;
			dirY = _dirY;
			
			var r:Number = Math.random();
			
			if (r < 0.5) {
			graphic = Image.createRect(6, 6, 0xF00000);
			} else if (r < 0.80) {
			graphic = Image.createRect(6, 6, 0xCC0000);
			} else { 
			graphic = Image.createRect(6, 6, 0xFFF000);
			}
			graphic.x -= 3;
			graphic.y -= 3;
			
			setHitbox(6, 6, 3, 3);
		}
		
		override public function update():void
		{
			x += dirX * speed;
			y += dirY * speed;
			speed *= friction;
			
		if (speed <= 2) {
				remove();
			}
			
		(graphic as Image).alpha = speed / 25;
		}
		
		public function remove():void
		{
			world.remove(this);
		}
	}

	
	
	
	
	
	
	
}