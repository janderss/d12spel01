package game.entities 
{
	import game.entities.weapons.Weapon;
	import game.entities.weapons.WeaponPistol;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;


	public class Enemy extends Entity
	{
		private var _health:Number = 3;
		
		public var alive:Boolean = true;
		
		public var speed:Number = 3
	

		public var vx:Number = 0;
		public var vy:Number = 0;
		
		public function Enemy()
		{
			
			super(0, 0);
			setSpawnPosition();
		

			
			
			
			graphic = Image.createRect(20, 20, 0xFF000F);
			graphic.x = -10;
			graphic.y = -10;
			
			setHitbox(20, 20, 10, 10);
			
			type = "enemy";
			
			// koll så att vi int krockar i na enemy så att vi fastnar
			// Ifall vi gör he så lägg ny position
			while (tooCloseToEnemy()) {
				setSpawnPosition();
			}

		}
		
				override public function update():void 
		{
				vx = 0;
				vy = 0;
			
				if(x < Globals.gameWorld.player.x) {
					vx += speed;
				}
				if(x > Globals.gameWorld.player.x) {
					vx -= speed;
				}
				if(y < Globals.gameWorld.player.y) {
					vy += speed;
				}
				if(y > Globals.gameWorld.player.y) {
					vy -= speed;
				}
				
				moveBy(vx, vy, ["player", "enemy"]);
				
		}
		public function get health():Number
		{
			return _health
		}
		
		public function set health(value:Number):void
		{
			_health = value;
			
			if (_health <= 0)
			{
				die();
			}
		}
		public function die():void
		{
			if(alive) {
			alive = false;
			world.remove(this);
			Globals.gameWorld.removeEnemy(this);
			}
		}
		
		private function setSpawnPosition():void
		{
			x = -20;
			y = 460 * Math.random();
			var r:Number = Math.random();
			
			if (r < 0.25) {
				x = 640 + 20;
			} else if (r < 0.5) {
				y = -20;
				x = 640 * Math.random();
			} else if (r < 0.75) {
				y = 640 + 20;
				x = 640 * Math.random();
			}	
			
		}
		
		private function tooCloseToEnemy():Boolean
		{
			for (var i:int = Globals.gameWorld.enemies.length - 1; i >= 0; i--)
			{
				if (FP.distance(x, y, 
				Globals.gameWorld.enemies[i].x, 
				Globals.gameWorld.enemies[i].y) < 20)
				{
				return true;	
				}
				
			}
			return false;
		}
		
	}
}











