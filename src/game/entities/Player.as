package game.entities 
{
	import game.entities.weapons.Weapon;
	import game.entities.weapons.WeaponAk;
	import game.entities.weapons.WeaponFlameGrenade;
	import game.entities.weapons.WeaponFlametrower;
	import game.entities.weapons.WeaponGrenadeLauncher;
	import game.entities.weapons.WeaponPistol;
	import game.entities.weapons.WeaponShotgun;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.Resources;

	public class Player extends Entity
	{
		public var speed:Number = 8
			
		public var vx:Number = 0;
		public var vy:Number = 0;
		
		
		public var weapon:Weapon;
		
		public var spritemap:Spritemap
		
		public function Player() 
		{
			super(100, 100);
			//graphic = new Image(Resources.imgPlayer);
			spritemap = new Spritemap(Resources.imgAnimation, 20, 20);
			graphic = spritemap;
			graphic.x = -10;
			graphic.y = -10;
			
			// add animation
			spritemap.add("default", [0, 1, 2], 6, true);
			spritemap.play("default");
			
			
			setHitbox(20, 20, 10, 10);
			
			type = "player";
			
			weapon = new WeaponShotgun(this, handleShooting);
		}
		
		override public function update():void 
		{
			
				vx = 0;
				vy = 0;
			
			if (Input.check(Key.D)) {
				vx += speed;
			}
			if (Input.check(Key.A)) {
				vx -= speed;
			}
			if (Input.check(Key.W)) {
				vy -= speed;
			}
			if (Input.check(Key.S)) {
				vy += speed;
			}
			
				moveBy(vx, vy, "enemy");
				
			if (weapon != null) {
					weapon.update();
			}
			// TODO : Flytta shoot kodin hid

			
			// Clamp to screen
			if (x < 10) x = 10;
			if (x > FP.width - 10) x = FP.width - 10;
			if (y < 10) y = 10;
			if (y > FP.height - 10) y = FP.height - 10;
		}	
			public function handleShooting():void
			{

			if (Input.check(Key.LEFT)) {
				weapon.shoot(-1, 0);
			}
			else if (Input.check(Key.DOWN)) {
				weapon.shoot(0, 1);
			}
			else if (Input.check(Key.RIGHT)) {
				weapon.shoot(1, 0);
			}
			else if (Input.check(Key.UP)) {
				weapon.shoot(0, -1);

			}
		}


	}

}